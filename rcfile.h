/* Copyright (C) 2000-2013 Boris Wesslowski */
/* $Id: rcfile.h 731 2013-05-17 14:15:23Z bw $ */

#ifndef _RCFILE_H
#define _RCFILE_H

unsigned char read_rcfile(char *rcfile, unsigned char must_exist, unsigned char type);

#endif
