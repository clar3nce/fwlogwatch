/* Copyright (C) 2000-2013 Boris Wesslowski */
/* $Id: netfilter.h 731 2013-05-17 14:15:23Z bw $ */

#ifndef _NETFILTER_H
#define _NETFILTER_H

unsigned char flex_netfilter(char *input, int linenum);

#endif
