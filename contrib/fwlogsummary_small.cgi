#!/bin/sh
# Copyright (C) 2000-2013 Boris Wesslowski
# $Id: fwlogsummary_small.cgi 731 2013-05-17 14:15:23Z bw $

echo "Content-Type: text/html"
echo
/usr/local/sbin/fwlogwatch -w -l 1h -z -s -d
