/* Copyright (C) 2000-2013 Boris Wesslowski */
/* $Id: netscreen.h 731 2013-05-17 14:15:23Z bw $ */

#ifndef _NETSCREEN_H
#define _NETSCREEN_H

unsigned char flex_netscreen(char *input, int linenum);

#endif
