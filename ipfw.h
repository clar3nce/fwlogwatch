/* Copyright (C) 2000-2013 Boris Wesslowski */
/* $Id: ipfw.h 731 2013-05-17 14:15:23Z bw $ */

#ifndef _IPFW_H
#define _IPFW_H

unsigned char flex_ipfw(char *input, int linenum);

#endif
