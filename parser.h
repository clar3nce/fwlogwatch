/* Copyright (C) 2000-2013 Boris Wesslowski */
/* $Id: parser.h 731 2013-05-17 14:15:23Z bw $ */

#ifndef _PARSER_H
#define _PARSER_H

unsigned char parse_line(char *input, int linenum);
int parse_time(char *input);
void select_parsers(void);

#endif
