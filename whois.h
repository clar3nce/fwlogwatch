/* Copyright (C) 2000-2013 Boris Wesslowski */
/* $Id: whois.h 731 2013-05-17 14:15:23Z bw $ */

#ifndef _WHOIS_H
#define _WHOIS_H

struct whois_entry *whois(struct in6_addr ip);
void whois_connect(const char *whois_server);
void whois_close(void);

#endif
