/* Copyright (C) 2000-2013 Boris Wesslowski */
/* $Id: modes.h 731 2013-05-17 14:15:23Z bw $ */

#ifndef _MODES_H
#define _MODES_H

void mode_summary(void);
void mode_rt_response(void);
void mode_show_log_times(void);

#endif
