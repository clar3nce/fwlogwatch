/* Copyright (C) 2000-2013 Boris Wesslowski */
/* $Id: snort.h 731 2013-05-17 14:15:23Z bw $ */

#ifndef _SNORT_H
#define _SNORT_H

unsigned char flex_snort(char *input, int linenum);

#endif
