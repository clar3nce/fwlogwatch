/* Copyright (C) 2000-2013 Boris Wesslowski */
/* $Id: net.h 731 2013-05-17 14:15:23Z bw $ */

#ifndef _NET_H
#define _NET_H

void prepare_socket(void);
void handshake(int linenum, int hitnum, int ignored);

#endif
