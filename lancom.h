/* Copyright (C) 2000-2013 Boris Wesslowski */
/* $Id: lancom.h 731 2013-05-17 14:15:23Z bw $ */

#ifndef _LANCOM_H
#define _LANCOM_H

unsigned char lancom(char *input, int linenum);

#endif
