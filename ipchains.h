/* Copyright (C) 2000-2013 Boris Wesslowski */
/* $Id: ipchains.h 731 2013-05-17 14:15:23Z bw $ */

#ifndef _IPCHAINS_H
#define _IPCHAINS_H

unsigned char flex_ipchains(char *input, int linenum);

#endif
